namespace Resource {
extern const nall::vector<uint8_t> Icon;
extern const nall::vector<uint8_t> Logo;
namespace System {
extern const nall::vector<uint8_t> Manifest;
extern const nall::vector<uint8_t> Boards;
extern const nall::vector<uint8_t> IPLROM;
}
}
